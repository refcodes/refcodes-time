// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.time;

/**
 * The Enum TimeUnit.
 *
 * @author steiner
 */
public enum TimeUnit {

	// @formatter:off
	NANOSECOND(1L),
	
	MILLISECOND(1000000L),
	
	SECOND(1000000000L),
	
	MINUTE(60000000000L),
	
	HOUR(3600000000000L),
	
	DAY(86400000000000L),
	
	YEAR(31536000000000000L);
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private long _milliseconds;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new time unit.
	 *
	 * @param aMilliseconds the milliseconds
	 */
	private TimeUnit( long aMilliseconds ) {
		_milliseconds = aMilliseconds;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the divisor for the time unit to be converted to seconds.
	 * 
	 * @return Divisor for the time unit to be converted to seconds.
	 */
	public long getMilliseconds() {
		return _milliseconds;
	}

	/**
	 * Converts a given time from one {@link TimeUnit} to this instance's time
	 * unit.
	 * 
	 * @param aFromTimeUnit The {@link TimeUnit} to convert from.
	 * @param aFromTime The time to be converted.
	 * 
	 * @return The converted time as of the current instance's time unit.
	 */
	public float toTime( TimeUnit aFromTimeUnit, long aFromTime ) {
		return toTimeUnit( aFromTimeUnit, aFromTime, this );
	}

	// /////////////////////////////////////////////////////////////////////////
	// UTILITIES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts a given time from one {@link TimeUnit} to another
	 * {@link TimeUnit}.
	 * 
	 * @param aFromTimeUnit The {@link TimeUnit} to convert from.
	 * @param aTime The time to be converted.
	 * @param aToTimeUnit The {@link TimeUnit} to convert to.
	 * 
	 * @return The converted time.
	 */
	public static float toTimeUnit( TimeUnit aFromTimeUnit, long aTime, TimeUnit aToTimeUnit ) {
		final long theMillis = aTime * aFromTimeUnit.getMilliseconds();
		return theMillis / aToTimeUnit.getMilliseconds();
	}

	/**
	 * Converts a given time from the provided {@link TimeUnit} to milliseconds.
	 * 
	 * @param aTimeUnit The {@link TimeUnit} to convert from.
	 * @param aTime The time to be converted to milliseconds.
	 * 
	 * @return The converted time in milliseconds.
	 */
	public static long toMilliseconds( TimeUnit aTimeUnit, long aTime ) {
		return aTime * aTimeUnit.getMilliseconds() / MILLISECOND.getMilliseconds();
	}
}
