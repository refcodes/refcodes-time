// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.time;

/**
 * Provides an accessor for a time property.
 */
public interface TimeAccessor {

	/**
	 * Retrieves the time from the time property.
	 * 
	 * @param aTimeUnit The time unit for the provided time.
	 * 
	 * @return The time stored by the time property.
	 */
	float getTime( TimeUnit aTimeUnit );

	/**
	 * Provides a mutator for a time property.
	 */
	public interface TimeMutator {

		/**
		 * Sets the time for the time property.
		 * 
		 * @param aTime The time to be stored by the time property.
		 * @param aTimeUnit The time unit for the provided time.
		 */
		void setTime( float aTime, TimeUnit aTimeUnit );
	}

	/**
	 * Provides a builder method for a time property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TimeBuilder<B extends TimeBuilder<B>> {

		/**
		 * Sets the time for the time property.
		 * 
		 * @param aTime The time to be stored by the time property.
		 * @param aTimeUnit The time unit for the provided time.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTime( float aTime, TimeUnit aTimeUnit );
	}

	/**
	 * Provides a time property.
	 */
	public interface TimeProperty extends TimeAccessor, TimeMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given time (setter) as of
		 * {@link #setTime(float, TimeUnit)} and returns the very same value
		 * (getter).
		 * 
		 * @param aTime The time to set (via {@link #setTime(float, TimeUnit)}).
		 * @param aTimeUnit The time unit for the provided time (as of
		 *        {@link #setTime(float, TimeUnit)}).
		 * 
		 * @return Returns the time passed for it to be used in conclusive
		 *         processing steps.
		 */
		default float letTime( float aTime, TimeUnit aTimeUnit ) {
			setTime( aTime, aTimeUnit );
			return aTime;
		}
	}
}
