// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.time;

import static java.time.temporal.ChronoField.*;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * The {@link DateFormat} contains <code>Date</code> and <code>DateFormat</code>
 * related values useful for parsing from and formatting to date {@link String}
 * instances.
 */
public enum DateFormat {

	// /////////////////////////////////////////////////////////////////////////
	// DATE FORMATS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Date format as used for cookies as of the Netscape cookie specification.
	 * See
	 * "https://de.wikipedia.org/wiki/HTTP-Cookie#Cookies_nach_Netscape-Spezifikation"
	 */
	NETSCAPE_COOKIE_DATE_FORMAT(DateTimeFormatter.ofPattern( "EEE',' dd-MMM-yyyy HH:mm:ss 'GMT'" ).withZone( ZoneId.of( "GMT" ) )),

	/**
	 * An alternate cookie date format also seen in documentations out there on
	 * the internet.
	 */
	ALTERNATE_COOKIE_DATE_FORMAT(DateTimeFormatter.ofPattern( "EEE',' dd MMM yyyy HH:mm:ss 'GMT'" ).withZone( ZoneId.of( "GMT" ) )),

	/**
	 * An alternate cookie date format also seen in documentations out there on
	 * the internet. Can format something like "Thu, 01-Jan-1970 00:00:10 GMT".
	 * Similar to the {@link DateTimeFormatter#RFC_1123_DATE_TIME} formatter.
	 */
	RFC_1123_DATE_TIME_ALIKE(toRfc1123DateTimeAlike()),

	/**
	 * Minimum sortable (first comes the year, last the day) date format,
	 * contains the date without the time.
	 */
	MIN_DATE_FORMAT(DateTimeFormatter.ofPattern( "yyyy-MM-dd" ).withZone( ZoneId.systemDefault() )),

	/**
	 * Normal sortable (first comes the year, last the seconds) date format,
	 * contains the date and the time till a granularity of seconds. The
	 * character 'T' separates the date portion from the time portion of the
	 * date format in order to unambiguously parse a such formatted date
	 * {@link String}.
	 */
	NORM_DATE_FORMAT(DateTimeFormatter.ofPattern( "yyyy-MM-dd'T'HH:mm:ss" ).withZone( ZoneId.systemDefault() )),

	/**
	 * Maximum sortable (first comes the year, last the nanoseconds) date
	 * format, contains the date and the time till a granularity of
	 * milliseconds.The character 'T' separates the date portion from the time
	 * portion of the date format in order to unambiguously parse a such
	 * formatted date {@link String}.
	 */
	MAX_DATE_FORMAT(DateTimeFormatter.ofPattern( "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS" ).withZone( ZoneId.systemDefault() )),

	/**
	 * Similar date format as {@link #NORM_DATE_FORMAT} though not avoiding the
	 * space between the date and the time portion of the date format.
	 */
	ALTERNATE_DATE_FORMAT(DateTimeFormatter.ofPattern( "yyyy-MM-dd HH:mm:ss" ).withZone( ZoneId.systemDefault() )),

	/**
	 * Date format as used in German speaking locations, first comes the day,
	 * last the year, all separated by a dot '.' character.
	 */
	DE_DATE_FORMAT(DateTimeFormatter.ofPattern( "dd.mm.yyyy" ).withZone( ZoneId.systemDefault() )),

	/**
	 * Date format usable for (parts of) a filename, e.g. for rotating log files
	 * or the such with a granularity of days.
	 */
	MIN_FILENAME_DATE_FORMAT(DateTimeFormatter.ofPattern( "yyyyMMdd" ).withZone( ZoneId.systemDefault() )),
	/**
	 * Date format usable for (parts of) a filename, e.g. for rotating log files
	 * or the such with a granularity of seconds.
	 */
	NORM_FILENAME_DATE_FORMAT(DateTimeFormatter.ofPattern( "yyyyMMdd'T'HHmmss" ).withZone( ZoneId.systemDefault() )),
	/**
	 * Date format usable for (parts of) a filename, e.g. for rotating log files
	 * or the such with a granularity of nanoseconds.
	 */
	MAX_FILENAME_DATE_FORMAT(DateTimeFormatter.ofPattern( "yyyyMMdd'T'HHmmssSSSSSSS" ).withZone( ZoneId.systemDefault() )),

	/**
	 * ISO8601, Z means "zero hour offset" also known as "Zulu time" (UTC).
	 */
	ISO_8601_ZULU_TIME(DateTimeFormatter.ofPattern( "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ).withZone( ZoneId.of( "UTC" ) )),

	/**
	 * The ISO-like date-time formatter that formats or parses a date-time with
	 * offset and zone, such as '2011-12-03T10:15:30+01:00[Europe/Paris]'.
	 */
	ISO_ZONED_DATE_TIME(DateTimeFormatter.ISO_ZONED_DATE_TIME.withZone( ZoneId.of( "UTC" ) )),

	/**
	 * The ISO date formatter that formats or parses a date without an offset,
	 * such as '2011-12-03'.
	 */
	ISO_LOCAL_DATE(DateTimeFormatter.ISO_LOCAL_DATE.withZone( ZoneId.systemDefault() )),

	/**
	 * The ISO date-time formatter that formats or parses a date-time without an
	 * offset, such as '2011-12-03T10:15:30'
	 */
	ISO_LOCAL_DATE_TIME(DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone( ZoneId.systemDefault() )),

	/**
	 * The ISO date-time formatter that formats or parses a date-time with an
	 * offset, such as '2011-12-03T10:15:30+01:00'.
	 */
	ISO_OFFSET_DATE_TIME(DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone( ZoneId.of( "UTC" ) ));

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// ////////////////////////////////////////////////////////////////////////

	private DateTimeFormatter _dateFormat;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// ////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new date format.
	 *
	 * @param aDateFormat the date format
	 */
	private DateFormat( DateTimeFormatter aDateFormat ) {
		_dateFormat = aDateFormat;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// ////////////////////////////////////////////////////////////////////////

	/**
	 * Parses something like "Thu, 01-Jan-1970 00:00:10 GMT".
	 */
	private static DateTimeFormatter toRfc1123DateTimeAlike() {
		final DateTimeFormatterBuilder theBuilder = new DateTimeFormatterBuilder();
		theBuilder.parseCaseInsensitive().parseLenient().optionalStart().appendText( DAY_OF_WEEK, getDow() ).appendLiteral( ", " ).optionalEnd().appendValue( DAY_OF_MONTH, 1, 2, SignStyle.NOT_NEGATIVE ).appendLiteral( '-' ).appendText( MONTH_OF_YEAR, getMoy() ).appendLiteral( '-' ).appendValue( YEAR, 4 ).appendLiteral( ' ' ).appendValue( HOUR_OF_DAY, 2 ).appendLiteral( ':' ).appendValue( MINUTE_OF_HOUR, 2 ).optionalStart().appendLiteral( ':' ).appendValue( SECOND_OF_MINUTE, 2 ).optionalEnd().appendLiteral( ' ' ).appendOffset( "+HHMM", "GMT" );
		return theBuilder.toFormatter( Locale.getDefault( Locale.Category.FORMAT ) );
	}

	/**
	 * Gets the formatter.
	 *
	 * @return the formatter
	 */
	public DateTimeFormatter getFormatter() {
		return _dateFormat;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static Map<Long, String> getDow() {
		final Map<Long, String> theDow = new HashMap<>();
		theDow.put( 1L, "Mon" );
		theDow.put( 2L, "Tue" );
		theDow.put( 3L, "Wed" );
		theDow.put( 4L, "Thu" );
		theDow.put( 5L, "Fri" );
		theDow.put( 6L, "Sat" );
		theDow.put( 7L, "Sun" );
		return theDow;
	}

	private static Map<Long, String> getMoy() {
		final Map<Long, String> moy = new HashMap<>();
		moy.put( 1L, "Jan" );
		moy.put( 2L, "Feb" );
		moy.put( 3L, "Mar" );
		moy.put( 4L, "Apr" );
		moy.put( 5L, "May" );
		moy.put( 6L, "Jun" );
		moy.put( 7L, "Jul" );
		moy.put( 8L, "Aug" );
		moy.put( 9L, "Sep" );
		moy.put( 10L, "Oct" );
		moy.put( 11L, "Nov" );
		moy.put( 12L, "Dec" );
		return moy;
	}
}
