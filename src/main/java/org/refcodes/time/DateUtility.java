// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.time;

import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

/**
 * The Class DateUtility.
 */
public final class DateUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private DateUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Parses a {@link String} to retrieve a {@link Date} from it, using the
	 * provided {@link DateTimeFormatter} instances. The first one being able to
	 * parse a {@link Date} from the {@link String} will be used and that
	 * {@link Date} is returned. If none {@link DateTimeFormatter} instances
	 * were able to parse the provided {@link String}, then the first (as being
	 * provided in the array) {@link DateTimeFormatter}'s parse exception is
	 * thrown.
	 * 
	 * @param aDateString The date {@link String} to be parsed.
	 * @param aDateTimeFormatters The {@link DateTimeFormatter} instances to be
	 *        tried out, starting with the first one.
	 * 
	 * @return A {@link Date} retrieved from the given {@link String}.
	 * 
	 * @throws DateTimeException Thrown in case none of the provided
	 *         {@link DateTimeFormatter} instances was able to parse the string.
	 *         Then the first caught {@link DateTimeException} is thrown.
	 */
	public static Date toDate( String aDateString, DateTimeFormatter[] aDateTimeFormatters ) {
		Exception theFirstException = null;
		TemporalAccessor eTemporal;
		for ( DateTimeFormatter eDateFormat : aDateTimeFormatters ) {
			try {
				try {
					eTemporal = eDateFormat.parse( aDateString );
					final Instant theInstant = Instant.from( eTemporal );
					return new Date( theInstant.toEpochMilli() );
				}
				catch ( Exception e ) {
					if ( theFirstException != null ) {
						theFirstException = e;
					}
					final LocalDate theLocalDate = LocalDate.parse( aDateString, eDateFormat );
					return Date.from( theLocalDate.atStartOfDay( ZoneId.systemDefault() ).toInstant() );
				}

			}
			catch ( Exception e ) {
				if ( theFirstException != null ) {
					theFirstException = e;
				}
			}
		}
		if ( theFirstException != null ) {
			if ( theFirstException instanceof DateTimeException ) {
				throw (DateTimeException) theFirstException;
			}
			throw new DateTimeException( "Unable to parse date for date string <" + aDateString + ">: " + theFirstException.getMessage(), theFirstException );
		}
		throw new DateTimeException( "Unable to parse date for date string <" + aDateString + ">." );
	}

	/**
	 * Parses a {@link String} to retrieve a {@link LocalDate} from it, using
	 * the provided {@link DateTimeFormatter} instances. The first one being
	 * able to parse a {@link LocalDate} from the {@link String} will be used
	 * and that {@link LocalDate} is returned. If none {@link DateTimeFormatter}
	 * instances were able to parse the provided {@link String}, then the first
	 * (as being provided in the array) {@link DateTimeFormatter}'s parse
	 * exception is thrown.
	 * 
	 * @param aDateString The date {@link String} to be parsed.
	 * @param aDateTimeFormatters The {@link DateTimeFormatter} instances to be
	 *        tried out, starting with the first one.
	 * 
	 * @return A {@link LocalDate} retrieved from the given {@link String}.
	 * 
	 * @throws DateTimeException Thrown in case none of the provided
	 *         {@link DateTimeFormatter} instances was able to parse the string.
	 *         Then the first caught {@link DateTimeException} is thrown.
	 */
	public static LocalDate toLocalDate( String aDateString, DateTimeFormatter[] aDateTimeFormatters ) {
		Exception theFirstException = null;
		for ( DateTimeFormatter eDateFormat : aDateTimeFormatters ) {
			try {
				return LocalDate.parse( aDateString, eDateFormat );
			}
			catch ( Exception e ) {
				if ( theFirstException != null ) {
					theFirstException = e;
				}
			}
		}
		if ( theFirstException != null ) {
			if ( theFirstException instanceof DateTimeException ) {
				throw (DateTimeException) theFirstException;
			}
			throw new DateTimeException( "Unable to parse date for date string <" + aDateString + ">: " + theFirstException.getMessage(), theFirstException );
		}
		throw new DateTimeException( "Unable to parse date for date string <" + aDateString + ">." );
	}

	/**
	 * Parses a {@link String} to retrieve a {@link LocalDateTime} from it,
	 * using the provided {@link DateTimeFormatter} instances. The first one
	 * being able to parse a {@link LocalDateTime} from the {@link String} will
	 * be used and that {@link LocalDateTime} is returned. If none
	 * {@link DateTimeFormatter} instances were able to parse the provided
	 * {@link String}, then the first (as being provided in the array)
	 * {@link DateTimeFormatter}'s parse exception is thrown.
	 * 
	 * @param aDateString The date {@link String} to be parsed.
	 * @param aDateTimeFormatters The {@link DateTimeFormatter} instances to be
	 *        tried out, starting with the first one.
	 * 
	 * @return A {@link LocalDateTime} retrieved from the given {@link String}.
	 * 
	 * @throws DateTimeException Thrown in case none of the provided
	 *         {@link DateTimeFormatter} instances was able to parse the string.
	 *         Then the first caught {@link DateTimeException} is thrown.
	 */
	public static LocalDateTime toLocalDateTime( String aDateString, DateTimeFormatter[] aDateTimeFormatters ) {
		Exception theFirstException = null;
		for ( DateTimeFormatter eDateFormat : aDateTimeFormatters ) {
			try {
				return LocalDateTime.parse( aDateString, eDateFormat );
			}
			catch ( Exception e ) {
				if ( theFirstException != null ) {
					theFirstException = e;
				}
			}
		}
		if ( theFirstException != null ) {
			if ( theFirstException instanceof DateTimeException ) {
				throw (DateTimeException) theFirstException;
			}
			throw new DateTimeException( "Unable to parse date for date string <" + aDateString + ">: " + theFirstException.getMessage(), theFirstException );
		}
		throw new DateTimeException( "Unable to parse date for date string <" + aDateString + ">." );
	}
}
