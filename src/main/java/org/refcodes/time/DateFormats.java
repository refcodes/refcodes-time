// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.time;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * The {@link DateFormats} represent sets of {@link DateFormat} definitions.
 * Such a set is most useful to parse back a {@link String} into a {@link Date}
 * when there are multiple possible {@link String} representations. E.g. a
 * cookie's date representation might be either as defined by the
 * {@link DateFormat#NETSCAPE_COOKIE_DATE_FORMAT} or by the
 * {@link DateFormat#ALTERNATE_COOKIE_DATE_FORMAT}.
 */
public enum DateFormats {

	// /////////////////////////////////////////////////////////////////////////
	// DATE FORMATS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sound set of common {@link DateFormat} definitions with no
	 * https://www.metacodes.pro addressee.
	 */
	DEFAULT_DATE_FORMATS(new DateTimeFormatter[] { DateFormat.NORM_DATE_FORMAT.getFormatter(), DateFormat.MIN_DATE_FORMAT.getFormatter(), DateFormat.ALTERNATE_DATE_FORMAT.getFormatter(), DateFormat.DE_DATE_FORMAT.getFormatter(), DateFormat.NETSCAPE_COOKIE_DATE_FORMAT.getFormatter() }),

	/**
	 * Sound set of common short {@link DateFormat} definitions with no
	 * https://www.metacodes.pro addressee.
	 */
	DEFAULT_DATE_FORMATS_SHORT(new DateTimeFormatter[] { DateFormat.MIN_DATE_FORMAT.getFormatter(), DateFormat.NORM_DATE_FORMAT.getFormatter(), DateFormat.ALTERNATE_DATE_FORMAT.getFormatter(), DateFormat.DE_DATE_FORMAT.getFormatter() }),

	/**
	 * Set of {@link DateFormat} definitions commonly used by cookies.
	 */
	COOKIE_DATE_FORMATS(new DateTimeFormatter[] { DateFormat.RFC_1123_DATE_TIME_ALIKE.getFormatter(), DateTimeFormatter.RFC_1123_DATE_TIME, DateFormat.NETSCAPE_COOKIE_DATE_FORMAT.getFormatter(), DateFormat.ALTERNATE_COOKIE_DATE_FORMAT.getFormatter() }),

	/**
	 * Set of {@link DateFormat} definitions found in heterogeneous internet
	 * related services.
	 */
	INTERNET_DATE_FORMATS(new DateTimeFormatter[] { DateFormat.ISO_ZONED_DATE_TIME.getFormatter(), DateFormat.ISO_OFFSET_DATE_TIME.getFormatter(), DateFormat.RFC_1123_DATE_TIME_ALIKE.getFormatter(), DateTimeFormatter.RFC_1123_DATE_TIME, DateFormat.NETSCAPE_COOKIE_DATE_FORMAT.getFormatter(), DateFormat.ALTERNATE_COOKIE_DATE_FORMAT.getFormatter() }),

	/**
	 * Set of common {@link DateFormat} ISO definitions.
	 */
	ISO_DATE_FORMATS(new DateTimeFormatter[] { DateFormat.ISO_8601_ZULU_TIME.getFormatter(), DateFormat.ISO_LOCAL_DATE.getFormatter(), DateFormat.ISO_LOCAL_DATE_TIME.getFormatter(), DateFormat.ISO_OFFSET_DATE_TIME.getFormatter(), DateFormat.ISO_ZONED_DATE_TIME.getFormatter() });

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private DateTimeFormatter[] _dateFormats;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// ////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new date formats.
	 *
	 * @param aDateFormats the date formats
	 */
	private DateFormats( DateTimeFormatter[] aDateFormats ) {
		_dateFormats = aDateFormats;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// ////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the date formats.
	 *
	 * @return the date formats
	 */
	public DateTimeFormatter[] getDateFormats() {
		return _dateFormats;
	}

	/**
	 * Creates a {@link Date} from the provided {@link String} using the date
	 * formats as retrieved by the {@link #getDateFormats()} method. If one date
	 * format fails, then the next one is used to parse the date text.
	 *
	 * @param aDateString The date text to be converted to a {@link Date}
	 *        instance.
	 * 
	 * @return The {@link Date} instance as of the date text.
	 * 
	 * @throws DateTimeException Thrown in case none of the provided
	 *         {@link DateTimeFormatter} instances was able to parse the string.
	 *         Then the first caught {@link DateTimeException} is thrown.
	 */
	public Date toDate( String aDateString ) {
		return DateUtility.toDate( aDateString, getDateFormats() );
	}

	/**
	 * Creates a {@link LocalDate} from the provided {@link String} using the
	 * date formats as retrieved by the {@link #getDateFormats()} method. If one
	 * date format fails, then the next one is used to parse the date text.
	 *
	 * @param aDateString The date text to be converted to a {@link Date}
	 *        instance.
	 * 
	 * @return The {@link Date} instance as of the date text.
	 * 
	 * @throws DateTimeException Thrown in case none of the provided
	 *         {@link DateTimeFormatter} instances was able to parse the string.
	 *         Then the first caught {@link DateTimeException} is thrown.
	 */
	public LocalDate toLocalDate( String aDateString ) {
		return DateUtility.toLocalDate( aDateString, getDateFormats() );
	}

	/**
	 * Creates a {@link LocalDateTime} from the provided {@link String} using
	 * the date formats as retrieved by the {@link #getDateFormats()} method. If
	 * one date format fails, then the next one is used to parse the date text.
	 *
	 * @param aDateString The date text to be converted to a {@link Date}
	 *        instance.
	 * 
	 * @return The {@link Date} instance as of the date text.
	 * 
	 * @throws DateTimeException Thrown in case none of the provided
	 *         {@link DateTimeFormatter} instances was able to parse the string.
	 *         Then the first caught {@link DateTimeException} is thrown.
	 */
	public LocalDateTime toLocalDateTime( String aDateString ) {
		return DateUtility.toLocalDateTime( aDateString, getDateFormats() );
	}

	/**
	 * Tries to create a {@link Date} by harnessing the
	 * {@link DateTimeFormatter} instances known by this enumeration.
	 * 
	 * @param aDateString The date text to be converted to a {@link Date}
	 *        instance.
	 * 
	 * @return The {@link Date} instance as of the date text.
	 * 
	 * @throws DateTimeException Thrown in case none of the provided
	 *         {@link DateTimeFormatter} instances was able to parse the string.
	 *         Then the first caught {@link DateTimeException} is thrown.
	 */
	public static Date asDate( String aDateString ) {
		DateTimeException theFirstException = null;
		for ( DateFormats eDateFormats : values() ) {
			try {
				return eDateFormats.toDate( aDateString );
			}
			catch ( DateTimeException e ) {
				if ( theFirstException != null ) {
					theFirstException = e;
				}
			}
		}
		if ( theFirstException != null ) {
			throw theFirstException;
		}
		throw new DateTimeException( "Unable to parse date for date string <" + aDateString + ">." );
	}

	/**
	 * Tries to create a {@link LocalDate} by harnessing the
	 * {@link DateTimeFormatter} instances known by this enumeration.
	 * 
	 * @param aDateString The date text to be converted to a {@link LocalDate}
	 *        instance.
	 * 
	 * @return The {@link LocalDate} instance as of the date text.
	 * 
	 * @throws DateTimeException Thrown in case none of the provided
	 *         {@link DateTimeFormatter} instances was able to parse the string.
	 *         Then the first caught {@link DateTimeException} is thrown.
	 */
	public static LocalDate asLocalDate( String aDateString ) {
		DateTimeException theFirstException = null;
		for ( DateFormats eDateFormats : values() ) {
			try {
				return eDateFormats.toLocalDate( aDateString );
			}
			catch ( DateTimeException e ) {
				if ( theFirstException != null ) {
					theFirstException = e;
				}
			}
		}
		if ( theFirstException != null ) {
			throw theFirstException;
		}
		throw new DateTimeException( "Unable to parse date for date string <" + aDateString + ">." );
	}

	/**
	 * Tries to create a {@link LocalDateTime} by harnessing the
	 * {@link DateTimeFormatter} instances known by this enumeration.
	 * 
	 * @param aDateString The date text to be converted to a {@link LocalDate}
	 *        instance.
	 * 
	 * @return The {@link LocalDateTime} instance as of the date text.
	 * 
	 * @throws DateTimeException Thrown in case none of the provided
	 *         {@link DateTimeFormatter} instances was able to parse the string.
	 *         Then the first caught {@link DateTimeException} is thrown.
	 */
	public static LocalDateTime asLocalDateTime( String aDateString ) {
		DateTimeException theFirstException = null;
		for ( DateFormats eDateFormats : values() ) {
			try {
				return eDateFormats.toLocalDateTime( aDateString );
			}
			catch ( DateTimeException e ) {
				if ( theFirstException != null ) {
					theFirstException = e;
				}
			}
		}
		if ( theFirstException != null ) {
			throw theFirstException;
		}
		throw new DateTimeException( "Unable to parse date for date string <" + aDateString + ">." );
	}
}
