// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.time;

import static org.junit.jupiter.api.Assertions.*;
import java.text.ParseException;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import org.junit.jupiter.api.Test;

public class DateFormatTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCookieDateFormat1() {
		final Date theDate = DateFormats.COOKIE_DATE_FORMATS.toDate( "Thu, 01-Jan-1970 00:00:10 GMT" );
		assertEquals( 10000, theDate.getTime() );
	}

	@Test
	public void testMinDateFormat() throws ParseException {
		final String theDateString = "2021-02-24";
		final TemporalAccessor theMinDate = DateFormat.MIN_DATE_FORMAT.getFormatter().parse( theDateString );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theMinDate );
		}
		final Date theDate = DateUtility.toDate( theDateString, DateFormats.DEFAULT_DATE_FORMATS.getDateFormats() );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theDate.toString() );
		}
	}
}
