// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.time;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * The Class TimeUnitTest.
 */
public class TimeUnitTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long ONE_YEAR_IN_SECONDS = 31536000;
	private static final long FOUR_DAYS_IN_MINUTES = 5760;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Test time conversion.
	 */
	@Test
	public void testTimeConversion() {
		final float theSeconds = TimeUnit.toTimeUnit( TimeUnit.YEAR, 1, TimeUnit.SECOND );
		assertEquals( ONE_YEAR_IN_SECONDS, theSeconds );
		final float theYear = TimeUnit.toTimeUnit( TimeUnit.SECOND, ONE_YEAR_IN_SECONDS, TimeUnit.YEAR );
		assertEquals( 1, theYear );
		final float theMinutes = TimeUnit.toTimeUnit( TimeUnit.DAY, 4, TimeUnit.MINUTE );
		assertEquals( FOUR_DAYS_IN_MINUTES, theMinutes );
		final float theDays = TimeUnit.toTimeUnit( TimeUnit.MINUTE, FOUR_DAYS_IN_MINUTES, TimeUnit.DAY );
		assertEquals( 4, theDays );
	}
}
